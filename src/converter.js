/**
 * 
 * @param {string} hex one or two characters
 * @returns {string} hex with twi characters
 */
const pad = (hex) => {
    return [hex.length === 1 ? "0" + hex : hex];
}

module.exports = {
    /**
     * Converts the RGB values to a Hex string
     * @param {number} red 0-255
     * @param {number} green 0-255
     * @param {number} blue 0-255
     * @returns {string}  hex value
     */
    rgbToHex: (red, green, blue) => {
        const redHex = red.toString(16);
        const greenHex = green.toString(16);
        const blueHex = blue.toString(16);
        const hex = "#" + pad(redHex) + pad(greenHex) + pad(blueHex);
        return hex;
    },
    hexToRgb: (hex) => {
        const redRGB = parseInt(hex.substring(1, 3), 16);
        const greenRGB = parseInt(hex.substring(3, 5), 16);
        const blueRGB = parseInt(hex.substring(5, 7), 16);
        const rgb = [redRGB, greenRGB, blueRGB];
        return rgb.join(' ,');
    }
};